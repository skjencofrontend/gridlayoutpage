import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialsModule} from "./share/materials.module";
import { ProductComponent } from './product/product.component';
import { RouterModule} from "@angular/router";
import { WelcomeComponent } from './welcome/welcome.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import { CssGridComponent } from './css-grid/css-grid.component';
import { GridAgainComponent } from './grid-again/grid-again.component';
import { GridAreaComponent } from './grid-area/grid-area.component';
import { ResponsiveGridComponent } from './responsive-grid/responsive-grid.component';



@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    WelcomeComponent,
    NavigationComponent,
    CssGridComponent,
    GridAgainComponent,
    GridAreaComponent,
    ResponsiveGridComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialsModule,
    RouterModule.forRoot([
      {path:'product', component: ProductComponent},
      {path:'grid', component: CssGridComponent},
      {path:'gridAgain', component: GridAgainComponent},
      {path:'responsiveGrid', component: ResponsiveGridComponent},
      {path:'gridArea', component: GridAreaComponent},
      {path:'', component: WelcomeComponent}
    ],{useHash:false})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
