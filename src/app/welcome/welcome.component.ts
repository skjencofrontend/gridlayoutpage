import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor() { }
  showFiller = false;
  public sidebarArrow = "east";
  public sidebarText = "Side Bar"

  ngOnInit(): void {
  }


  toggleClicked() {
    if(this.sidebarArrow==="east"){
      this.sidebarArrow = "west";
    }
    else {
      this.sidebarArrow = "east";
    }
  }
}
